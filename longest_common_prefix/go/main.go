package main

func hasPrefix(prefix string, index int, strs []string) bool {
	for _, s := range strs {
		if len(s)-1 < index {
			return false
		}
		if string(s[index]) != prefix {
			return false
		}
	}
	return true
}

func longestCommonPrefix(strs []string) string {
	if len(strs) == 0 || strs[0] == "" {
		return ""
	}
	longestPrefix := ""
	for i := 0; i < len(strs[0]); i++ {
		prefix := strs[0][i]
		if hasPrefix(string(prefix), i, strs) {
			longestPrefix = longestPrefix + string(prefix)
		} else {
			return longestPrefix
		}
	}
	return longestPrefix
}
