package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_longestCommonPrefix(t *testing.T) {
	inputs := [][]string{{"flower", "flow", "flight"},
		{"dog", "racecar", "car"},
		{"ci", "car"}}
	expected := []string{"fl", "", "c"}
	for i := range inputs {
		assert.Equal(t, longestCommonPrefix(inputs[i]), expected[i])

	}

}
