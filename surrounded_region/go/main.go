package main

import "fmt"

// stack
type StackInterface struct {
	values []interface{}
}

func NewStackInterface() *StackInterface {
	return &StackInterface{
		values: []interface{}{},
	}
}
func (s *StackInterface) Push(val interface{}) {
	s.values = append(s.values, val)
}

func (s *StackInterface) Pop() interface{} {
	if len(s.values) == 0 {
		return 0
	}
	val := s.values[len(s.values)-1]
	s.values = s.values[:len(s.values)-1]
	return val
}

func (s *StackInterface) Peek() interface{} {
	if len(s.values) == 0 {
		return nil
	}
	val := s.values[len(s.values)-1]
	return val
}

func (s *StackInterface) IsEmpty() bool {
	return len(s.values) == 0
}

type nodeInfo struct {
	row int
	col int
}

func isBorder(board [][]byte, row int, col int) bool {
	if row == 0 || col == 0 || row == len(board)-1 || col == len(board[row])-1 {
		return true
	}
	return false
}

func key(row int, col int) string {
	return fmt.Sprintf("%d_%d", row, col)
}

func isO(board [][]byte, row int, col int) bool {
	return string(board[row][col]) == "O"
}
func markRegion(board [][]byte, row int, col int) {
	if row < 0 || row >= len(board) || col < 0 || col >= len(board[row]) {
		return
	}
	if isO(board, row, col) {
		board[row][col] = "*"[0]
	}

	if row > 0 && isO(board, row-1, col) {
		markRegion(board, row-1, col) //top
	}

	if row < len(board)-1 && isO(board, row+1, col) {
		markRegion(board, row+1, col) //bottom
	}

	if col > 0 && isO(board, row, col-1) {
		markRegion(board, row, col-1) //left
	}

	if col < len(board[row])-1 && isO(board, row, col+1) {
		markRegion(board, row, col+1) //right
	}

}

func markRegions(board [][]byte) {

	columns := len(board[0])
	for i := range board {
		if string(board[i][0]) == "O" {
			markRegion(board, i, 0)
		}
		if string(board[i][columns-1]) == "O" {
			markRegion(board, i, columns-1)
		}
	}

	for col:=0;col<columns;col++ {
		if string(board[0][col]) == "O" {
			markRegion(board, 0, col)
		}
		if string(board[len(board)-1][col]) == "O" {
			markRegion(board, len(board)-1, col)
		}
	}
}


func _solve(board [][]byte) [][]byte {
	if board == nil || len(board) ==0 {
		return board
	}
	markRegions(board)
	for row := range board {
		for col := range board[row] {
			if string(board[row][col]) == "O" {
				board[row][col] ="X"[0]
			}else if string(board[row][col])=="*" {
				board[row][col] ="O"[0]
			}
		}
	}
	return board
}
func solve(board [][]byte) {
	_solve(board)
}
func main() {
	fmt.Println("hello")
}
