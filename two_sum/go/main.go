package main

import "fmt"

func getKeys(val map[int]bool) []int {
	var result []int
	for k := range val {
		result = append(result, k)
	}
	return result
}

func twoSum(nums []int, target int) []int {
	result := make(map[int]bool, len(nums))
	for i := 0; i < len(nums); i++ {
		for j := 1; j < len(nums); j++ {
			if nums[i]+nums[j] == target && i != j {
				result[i] = true
				result[j] = true
			}
		}
	}
	return getKeys(result)
}

type Val struct {
	val int
}

func twoSumOnePass(nums []int, target int) []int {
	lookupMap := make(map[int]*Val, len(nums))
	var result []int
	for i, num := range nums {
		otherNumber := target - num
		if lookupMap[otherNumber] != nil {
			return []int{lookupMap[otherNumber].val, i}
		} else if lookupMap[num] == nil {
			lookupMap[num] = &Val{val: i}
		}
	}
	return result
}

func main() {
	//fmt.Println(twoSumOnePass([]int{2, 7, 11, 15}, 9))
	//fmt.Println(twoSumOnePass([]int{3, 2, 4}, 6))
	//fmt.Println(twoSumOnePass([]int{3, 3}, 6))
	//fmt.Println(twoSumOnePass([]int{-1, -2, -3, -4, -5}, -8))

	fmt.Println(twoSumOnePass([]int{3, 3, 4}, 7))

}
