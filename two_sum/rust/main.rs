use std::collections::{HashMap, HashSet};

struct Solution {}

impl Solution {
    pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
        let mut result: HashSet<i32> = HashSet::<i32>::new();
        for i in 0..nums.len() {
            for j in 1..nums.len() {
                if nums[i] + nums[j] == target && i != j {
                    result.insert(i as i32);
                    result.insert(j as i32);
                }
            }
        }
        return result.into_iter().collect();
    }
    pub fn two_sum_one_pass(nums: Vec<i32>, target: i32) -> Vec<i32> {
        let mut lookup_map: HashMap<i32, i32> = HashMap::<i32, i32>::new();
        let mut result: Vec<i32> = Vec::<i32>::new();
        for i in 0..nums.len() {
            let other_number = target - nums[i];
            if lookup_map.contains_key(&other_number) {
                result.push(i as i32);
                result.push(lookup_map[&other_number]);
            } else {
                lookup_map.insert(nums[i], i as i32);
            }
        }
        return result;
    }
}

fn main() {
    println!("{:?}", Solution::two_sum_one_pass(vec![2, 7, 11, 15], 9));
    println!("{:?}", Solution::two_sum_one_pass(vec![3, 2, 4], 6));
    println!("{:?}", Solution::two_sum_one_pass(vec![3, 3], 6));
    println!(
        "{:?}",
        Solution::two_sum_one_pass(vec![-1, -2, -3, -4, -5], -8)
    );
    println!("{:?}", Solution::two_sum_one_pass(vec![2, 7, 11, 15], 9));
}
