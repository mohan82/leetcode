package main

func transpose(matrix [][]int) [][]int {
	for i := 0; i < len(matrix); i++ {
		for j := i; j < len(matrix[i]); j++ {
			tmp := matrix[i][j]
			matrix[i][j] = matrix[j][i]
			matrix[j][i] = tmp
		}
	}
	return matrix
}

//
// Input
//[ 1 2 3
//  4 5 6
//  7 8 9
//]
//// Transpose
//    [1 4 7
//     2 5 8
//     3 6 9 ]

//   Reverse
//       [7 4  1
//        8 5  2
//        9 6  3]

func reverse(matrix [][]int) [][]int {
	for row := 0; row < len(matrix); row++ {
		for col := 0; col < len(matrix[row])/2; col++ {
			swapCol := (len(matrix[row]) - 1) - col
			tmp := matrix[row][col]
			matrix[row][col] = matrix[row][swapCol]
			matrix[row][swapCol] = tmp
		}
	}

	return matrix
}

func rotate(matrix [][]int) {
	reverse(transpose(matrix))
}
