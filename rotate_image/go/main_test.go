package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_transpose(t *testing.T) {
	matrix := [][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
	transpose(matrix)
	expected := [][]int{{1, 4, 7}, {2, 5, 8}, {3, 6, 9}}
	assert.Equal(t, matrix, expected)
}

func Test_transposeWith4DArr(t *testing.T) {
	matrix := [][]int{{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}}
	transpose(matrix)
	expected := [][]int{{5, 2, 13, 15}, {1, 4, 3, 14}, {9, 8, 6, 12}, {11, 10, 7, 16}}
	assert.Equal(t, matrix, expected)
}

func Test_reverse(t *testing.T) {
	matrix := [][]int{{1, 4, 7}, {2, 5, 8}, {3, 6, 9}}
	reverse(matrix)
	expected := [][]int{{7, 4, 1}, {8, 5, 2}, {9, 6, 3}}
	assert.Equal(t, matrix, expected)
}

func Test_ReverseWith4DArr(t *testing.T) {
	matrix := [][]int{{5, 2, 13, 15}, {1, 4, 3, 14}, {9, 8, 6, 12}, {11, 10, 7, 16}}
	reverse(matrix)
	expected := [][]int{{15, 13, 2, 5}, {14, 3, 4, 1}, {12, 6, 8, 9}, {16, 7, 10, 11}}
	assert.Equal(t, matrix, expected)
}

func Test_Rotate(t *testing.T) {
	matrix := [][]int{{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}}
	rotate(matrix)
	expected := [][]int{{15, 13, 2, 5}, {14, 3, 4, 1}, {12, 6, 8, 9}, {16, 7, 10, 11}}
	assert.Equal(t, matrix, expected)
}

func Test_RotateWithOneArg(t *testing.T) {
	matrix := [][]int{{1}}
	rotate(matrix)
	assert.Equal(t, matrix, matrix)

}
