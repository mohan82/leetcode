package main

import (
	"fmt"
	"math/rand"
)

func mergeTwoArrays(nums1 []int, nums2 []int) []int {
	var result []int
	for _, i := range nums1 {
		result = append(result, i)
	}
	for _, j := range nums2 {
		result = append(result, j)
	}
	return result
}

func quicksort(a []int) []int {
	if len(a) < 2 {
		return a
	}
	left, right := 0, len(a)-1
	pivot := rand.Int() % len(a)
	a[pivot], a[right] = a[right], a[pivot]

	for i, _ := range a {
		if a[i] < a[right] {
			a[left], a[i] = a[i], a[left]
			left++
		}
	}

	a[left], a[right] = a[right], a[left]

	quicksort(a[:left])
	quicksort(a[left+1:])

	return a
}
func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	mergedArray := mergeTwoArrays(nums1, nums2)
	mergedArray = quicksort(mergedArray)
	if len(mergedArray)%2 != 0 {
		mid := len(mergedArray) / 2
		return float64(mergedArray[mid])
	} else {
		mid1 := len(mergedArray) / 2
		mid2 := mid1 - 1
		return (float64(mergedArray[mid1]) + float64(mergedArray[mid2])) / float64(2)
	}
}
func main() {
	fmt.Println(findMedianSortedArrays([]int{1, 2}, []int{4, 7, 9000}))
	//fmt.Println(findMedianSortedArrays([]int{1, 2}, []int{4, 7}))
	//fmt.Println(findMedianSortedArrays([]int{1, 2}, []int{3, 4}))

}
