use std::fmt::Display;

#[derive(Debug)]
pub struct Queue<T: Display> {
    vec: Vec<T>,
}

impl<T: Display> Queue<T> {
    pub fn new() -> Queue<T> {
        Queue { vec: vec![] }
    }

    pub fn enqueue(&mut self, val: T) {
        self.vec.push(val)
    }
    pub fn dequeue(&mut self) -> Option<T> {
        if self.vec.is_empty() {
            return None;
        }
        Some(self.vec.remove(0))
    }

    pub fn is_empty(&self) -> bool {
        self.vec.is_empty()
    }
    pub fn print(&self) {
        for v in self.vec.iter() {
            println!("{}", v);
        }
    }
}
