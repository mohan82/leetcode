use std::cmp::PartialOrd;
use std::fmt;
use std::fmt::Display;

use crate::queue::Queue;
use crate::stack::Stack;

#[derive(Debug)]
pub struct Node<T: PartialOrd + Display> {
    pub val: T,
    left: Option<Box<Node<T>>>,
    right: Option<Box<Node<T>>>,
}

impl<T: PartialOrd + Display> std::fmt::Display for Node<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.val)
    }
}

impl<T: PartialOrd + Display> Node<T> {
    pub fn new(val: T) -> Node<T> {
        Node {
            val,
            left: None,
            right: None,
        }
    }
    pub fn insert(&mut self, val: T) {
        if val <= self.val {
            if self.left.is_none() {
                self.left = Some(Box::new(Node::new(val)));
            } else {
                self.left.as_mut().unwrap().insert(val)
            }
        } else {
            if self.right.is_none() {
                self.right = Some(Box::new(Node::new(val)));
            } else {
                self.right.as_mut().unwrap().insert(val)
            }
        }
    }
    pub fn find(&self, val: T) -> bool {
        if self.val == val {
            return true;
        } else if self.val < val {
            if self.left.is_none() {
                return false;
            }
            return self.left.as_ref().unwrap().find(val);
        } else {
            if self.right.is_none() {
                return false;
            }
            return self.right.as_ref().unwrap().find(val);
        }
    }

    pub fn print(root: &Node<T>) {
        if root.left.is_some() {
            println!("left");
            Node::print(root.left.as_ref().unwrap());
        }
        println!("{}", root.val);
        if root.right.is_some() {
            println!("right");
            Node::print(root.right.as_ref().unwrap());
        }
    }

    pub fn bfs(root: &Node<T>) {
        let mut q = Queue::new();
        let mut node = root;
        q.enqueue(node);
        while !q.is_empty() {
            node = q.dequeue().unwrap();
            println!("{}", node.val);
            if node.left.is_some() {
                q.enqueue(node.left.as_ref().unwrap());
            }
            if node.right.is_some() {
                q.enqueue(node.right.as_ref().unwrap());
            }
        }
    }

    pub fn dfs(root: &Node<T>) {
        let mut st = Stack::new();
        let mut node = root;
        st.push(node);
        while !st.is_empty() {
            node = st.pop().unwrap();
            println!("{}", node.val);
            if node.right.is_some() {
                st.push(node.right.as_ref().unwrap());
            }
            if node.left.is_some() {
                st.push(node.left.as_ref().unwrap());
            }
        }
    }
}
