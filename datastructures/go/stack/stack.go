package stack

type StackInt struct {
	values []int
}

func NewStackInt() *StackInt {
	return &StackInt{
		values: []int{},
	}
}

func (s *StackInt) Push(val int) {
	s.values = append(s.values, val)
}

func (s *StackInt) Pop() int {
	if len(s.values) == 0 {
		return 0
	}
	val := s.values[len(s.values)-1]
	s.values = s.values[:len(s.values)-1]
	return val
}

type StackString struct {
	values []string
}

func NewStackString() *StackString {
	return &StackString{
		values: []string{},
	}
}

func (s *StackString) Push(val string) {
	s.values = append(s.values, val)
}

func (s *StackString) Pop() string {
	if len(s.values) == 0 {
		return ""
	}
	val := s.values[len(s.values)-1]
	s.values = s.values[:len(s.values)-1]
	return val
}

type StackInterface struct {
	values []interface{}
}

func NewStackInterface() *StackInterface {
	return &StackInterface{
		values: []interface{}{},
	}
}
func (s *StackInterface) Push(val interface{}) {
	s.values = append(s.values, val)
}

func (s *StackInterface) Pop() interface{} {
	if len(s.values) == 0 {
		return 0
	}
	val := s.values[len(s.values)-1]
	s.values = s.values[:len(s.values)-1]
	return val
}

func (s *StackInterface) Peek() interface{} {
	if len(s.values) == 0 {
		return nil
	}
	val := s.values[len(s.values)-1]
	return val
}

func (s *StackInterface) IsEmpty() bool {
	return len(s.values) == 0
}
