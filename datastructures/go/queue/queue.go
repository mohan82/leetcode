package queue

type QueueInt struct {
	values []int
}

func NewQueueInt() *QueueInt {
	return &QueueInt{
		values: []int{},
	}
}

func (s *QueueInt) Enqueue(val int) {
	s.values = append(s.values, val)
}

func (s *QueueInt) Dequeue() int {
	if len(s.values) == 0 {
		return 0
	}
	val := s.values[0]
	s.values = append(s.values[:0], s.values[1:]...)
	return val
}

func (s *QueueInt) isEmpty() bool {
	return len(s.values) == 0
}

type QueueString struct {
	values []string
}

func NewQueueString() *QueueString {
	return &QueueString{
		values: []string{},
	}
}

func (s *QueueString) Enqueue(val string) {
	s.values = append(s.values, val)
}

func (s *QueueString) Dequeue() string {
	if len(s.values) == 0 {
		return ""
	}
	val := s.values[0]
	s.values = append(s.values[:0], s.values[1:]...)
	return val
}

func (s *QueueString) isEmpty() bool {
	return len(s.values) == 0
}

type QueueInterface struct {
	values []interface{}
}

func NewQueueInterface() *QueueInterface {
	return &QueueInterface{
		values: []interface{}{},
	}
}

func (s *QueueInterface) Enqueue(val interface{}) {
	s.values = append(s.values, val)
}

func (s *QueueInterface) Dequeue() interface{} {
	if len(s.values) == 0 {
		return nil
	}
	val := s.values[0]
	s.values = append(s.values[:0], s.values[1:]...)
	return val
}

func (s *QueueInterface) IsEmpty() bool {
	return len(s.values) == 0
}

func (s *QueueInterface) Peek() interface{}  {
	if len(s.values) == 0 {
		return nil
	}
	val := s.values[0]
	return val
}