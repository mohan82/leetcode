package tree

import (
	"fmt"
	"math"

	"leetcode/ds/queue"
	"leetcode/ds/stack"
)

type BtreeInt struct {
	Val   int
	left  *BtreeInt
	right *BtreeInt
}

func GetBtreeLevel(index int) int {
	return int(math.Floor(float64(index) - 1/2))
}

func FindRightNodeIndex(index int) int {
	return (2 * index) + 2
}

func FindLeftNodeIndex(index int) int {
	return (2 * index) + 1
}

func FindParentIndex(index int) int {
	return GetBtreeLevel(index)
}

func IsExist(index int, arr []int) bool {
	return index > 0 && index < len(arr)
}

func NewBtree(Val int) *BtreeInt {
	return &BtreeInt{
		Val: Val,
	}
}

type BtreeNodeIntFunc func(node *BtreeInt)
func (b *BtreeInt) dfs(f BtreeNodeIntFunc) {
	if b == nil {
		return
	}
	st := stack.NewStackInterface()
	st.Push(b)
	for !st.IsEmpty() {
		elem := st.Pop().(*BtreeInt)
		f(elem)
		if elem.right != nil {
			st.Push(elem.right)
		}
		if elem.left != nil {
			st.Push(elem.left)

		}
	}
}


func (b *BtreeInt) bfs(f BtreeNodeIntFunc) {
	if b == nil {
		return
	}
	q := queue.NewQueueInterface()
	q.Enqueue(b)
	for !q.IsEmpty() {
		elem := q.Dequeue().(*BtreeInt)
		f(elem)
		if elem.right != nil {
			q.Enqueue(elem.right)
		}
		if elem.left != nil {
			q.Enqueue(elem.left)

		}
	}
}


func (b *BtreeInt) print(nodeType string) {
	if b.left != nil {
		b.left.print("left")
	}
	fmt.Println(fmt.Sprintf("%s:%d", nodeType, b.Val))
	if b.right != nil {
		b.right.print("right")
	}
}


func NewBtreeWithArr(arr []int) *BtreeInt {
	if len(arr) == 0 {
		return nil
	}
	root := NewBtree(arr[0])
	q := queue.NewQueueInterface()
	q.Enqueue(root)
	for i := 0; !q.IsEmpty(); i++ {
		current := q.Dequeue().(*BtreeInt)
		if leftIndex := FindLeftNodeIndex(i); IsExist(leftIndex, arr) && arr[leftIndex]!=0 {
			current.left = NewBtree(arr[leftIndex])
			q.Enqueue(current.left)
		}
		if rightIndex := FindRightNodeIndex(i); IsExist(rightIndex, arr) && arr[rightIndex] !=0 {
			current.right = NewBtree(arr[rightIndex])
			q.Enqueue(current.right)
		}
	}
	return root
}
