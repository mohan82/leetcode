package tree

import (
	"fmt"
	"testing"
)

func TestNewWithArr(t *testing.T) {
	input := []int{3, 9, 20, 0, 0, 15, 7}
    tree:= NewBtreeWithArr(input)
    tree.bfs(func(node *BtreeInt){
    	fmt.Println(node.Val)
	})
}