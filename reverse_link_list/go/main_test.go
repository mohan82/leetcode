package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_reverseList(t *testing.T) {

	l:= &ListNode{1,&ListNode{2,
		&ListNode{3,&ListNode{4,nil}}}}
	expected:= &ListNode{4,&ListNode{3,
		&ListNode{2,&ListNode{1,
			nil}}}}
	assert.Equal(t,reverseList(l),expected)
}