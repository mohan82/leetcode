package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_isPalindrome(t *testing.T) {
   assert.Equal(t,isPalindrome(0),true)
	assert.Equal(t,isPalindrome(542),false)
	assert.Equal(t,isPalindrome(121),true)
	assert.Equal(t,isPalindrome(-121),false)

}