package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverse(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	var rev *ListNode
	current := head
	for current != nil {
		n := &ListNode{Val: current.Val, Next: rev}
		current = current.Next
		rev = n
	}
	return rev
}

func isEqual(l1 *ListNode, l2 *ListNode) bool {
	for l1.Next != nil && l2.Next != nil {
		if l2 == nil {
			return false
		}
		if l1 == nil {
			return false
		}
		if l1.Val != l2.Val {
			return false
		}
		l1 = l1.Next
		l2 = l2.Next
	}
	return true
}

func isPalindromeTwoPass(head *ListNode) bool {
	if head == nil {
		return true
	}
	r := reverse(head)
	return isEqual(head, r)
}

func isPalindrome(head *ListNode) bool {
	if head == nil || head.Next == nil {
		return true
	}
	slow, fast := head, head
	for fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}
	fast = head
	rev := reverse(slow)
	for rev != nil {
		if fast.Val != rev.Val {
			return false
		}
		rev = rev.Next
		fast = fast.Next
	}
	return true
}
