use std::option::Option::None;

#[derive(Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    pub fn new(val: i32) -> Self {
        return ListNode { next: None, val };
    }
}

pub struct Solution {}

type MyList = Option<Box<ListNode>>;

impl Solution {
    pub fn option(val: i32) -> Option<Box<ListNode>> {
        Option::Some(Box::new(ListNode::new(val)))
    }

    pub fn add_two_non_nil(
        l1: Option<Box<ListNode>>,
        l2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        let mut l1 = &l1;
        let mut l2 = &l2;
        let mut head = None;
        let mut cur = &mut head;
        let mut carry = 0;
        while l1.is_some() || l2.is_some() {
            let mut sum = carry;
            if let Some(node) = l1 {
                sum = sum + node.val;
                l1 = &node.next;
            }
            if let Some(node) = l2 {
                sum = sum + node.val;
                l2 = &node.next;
            }
            carry = sum / 10;
            *cur = Some(Box::new(ListNode::new(sum % 10)));
            cur = &mut cur.as_mut().unwrap().next;
        }
        if carry > 0 {
            *cur = Some(Box::new(ListNode::new(carry)));
        }
        return head;
    }
    pub fn add_two(l1: Option<Box<ListNode>>, l2: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        if l1.is_none() && l2.is_none() {
            return None;
        } else if l1.is_none() {
            return l2;
        } else if l2.is_none() {
            return l1;
        }
        return Solution::add_two_non_nil(l1, l2);
    }
}

fn to_list(v: Vec<i32>) -> MyList {
    let mut cur = None;
    for num in v.iter().rev() {
        let mut new_node = ListNode::new(*num);
        new_node.next = cur;
        cur = Some(Box::new(new_node));
    }
    cur
}

fn main() {
    let l3 = Solution::add_two(to_list(vec![9, 9, 9]), to_list(vec![9, 9, 9]));
    println!("{:?}", l3)
}
