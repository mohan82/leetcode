package main

import (
	"fmt"
	"math/big"
)

type ListNode struct {
	Val  int
	Next *ListNode
}

func (l *ListNode) String() string {
	if l == nil {
		return ""
	}
	s := ""
	for i := l; i != nil; i = i.Next {
		s = fmt.Sprintf("%s%d", s, i.Val)
	}
	return s
}

func toArray(l *ListNode) []int64 {
	var result []int64
	for i := l; i != nil; i = i.Next {
		result = append(result, int64(i.Val))
	}
	return result
}

func toNumber(digits []int64) *big.Int {
	result := new(big.Int)
	for i := len(digits); i > 0; i-- {
		elem := int64(i - 1)
		var pow big.Int
		pow.Exp(big.NewInt(10), big.NewInt(elem), nil)
		var out big.Int
		val := out.Mul(big.NewInt(digits[elem]), &pow)
		//result = result.Add(digits[elem]*int64(math.Pow10(elem)))
		result.Add(result, val)
	}
	return result
}
func toList(number *big.Int) *ListNode {
	var result []*big.Int
	if number.Cmp(big.NewInt(0)) == 0 {
		result = append(result, number)
	}
	for i := number; i.Cmp(big.NewInt(0)) >= 1; i.Div(i, big.NewInt(10)) {
		val := new(big.Int).Set(i)
		result = append(result, val.Mod(val, big.NewInt(10)))
	}
	var list *ListNode
	lastVisited := list
	for _, val := range result {
		l := &ListNode{Val: int(val.Int64()), Next: nil}
		if list == nil {
			lastVisited = l
			list = lastVisited
		} else {
			lastVisited.Next = l
			lastVisited = l
		}
	}
	return list

}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	var result big.Int
	n1 := toNumber(toArray(l1))
	n2 := toNumber(toArray(l2))
	total := result.Add(n1, n2)
	return toList(total)
}

func insert(a *ListNode, val int) *ListNode {
	if a == nil {
		return &ListNode{Val: val, Next: nil}
	}
	l := &ListNode{Val: val, Next: nil}
	a.Next = l
	return l
}

func next(l *ListNode) *ListNode {
	if l == nil {
		return nil
	}
	return l.Next
}

func addTwoNonNilList(l1 *ListNode, l2 *ListNode) *ListNode {
	var result, root *ListNode
	rem := 0
	for i, j := l1, l2; i != nil || j != nil; i, j = next(i), next(j) {
		c := rem
		rem = 0
		if i != nil {
			c = c + i.Val
		}
		if j != nil {
			c = c + j.Val
		}
		if c < 10 {
			result = insert(result, c)
			if root == nil {
				root = result
			}
		} else {
			valToInsert := c % 10
			result = insert(result, valToInsert)
			if root == nil {
				root = result
			}
			rem = c / 10
		}
	}
	if rem > 0 {
		result = insert(result, rem)
	}
	return root
}
func addTwoNumberSimple(l1 *ListNode, l2 *ListNode) *ListNode {
	if l1 == nil && l2 == nil {
		return nil
	}
	if l1 == nil {
		return l2
	}
	if l2 == nil {
		return l1
	}
	return addTwoNonNilList(l1, l2)

}
func main() {
	//number1 := &ListNode{2, &ListNode{4, &ListNode{3, nil}}}
	//number2 := &ListNode{1, &ListNode{0, &ListNode{0, nil}}}

	number1 := &ListNode{9, &ListNode{9, &ListNode{9, nil}}}
	number2 := &ListNode{9, &ListNode{9, &ListNode{9, nil}}}
	fmt.Println(addTwoNumbers(number1, number2))
	fmt.Println(addTwoNumberSimple(number1, number2))

}
