package main

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func maxSubArray(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	if len(nums) == 1 {
		return nums[0]
	}
	dp := make([]int, len(nums))
	dp[len(nums)-1] = nums[len(nums)-1]
	maxValue := dp[len(nums)-1]
	for i := len(nums) - 2; i >= 0; i-- {
		dp[i] = max(nums[i], nums[i]+dp[i+1])
		maxValue = max(maxValue, dp[i])
	}
	return maxValue
}

func maxSubArrayWithSpaceOptimisation(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	if len(nums) == 1 {
		return nums[0]
	}
	previousValue := nums[len(nums)-1]
	maxValue := previousValue
	for i := len(nums) - 2; i >= 0; i-- {
		res := max(nums[i], nums[i]+previousValue)
		maxValue = max(maxValue, res)
		previousValue = res
	}
	return maxValue
}
