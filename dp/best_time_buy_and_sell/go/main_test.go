package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_maxProfit(t *testing.T) {
	inputs := [][]int{{7, 1, 5}, {7, 1, 5, 3, 6, 4}, {7, 6, 4, 3, 1}, {2, 1}, {1, 4, 2}}
	expected := []int{4, 5, 0, 0, 3}
	for i := range inputs {
		assert.Equal(t, maxProfit(inputs[i]), expected[i])
	}
}

/*
        7,1

1   7
     1
 f2 =  max(f(1),num- num-2, num- num-1)

    7,1 ,5
      7       1
    1   5        5


   7,1,5 , 8

f3   = max(f(2), num-num-1,num-num-2, num-3)

          7           1     5
       1   5  8    5    8    8



*/
