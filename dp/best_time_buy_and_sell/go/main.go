package main

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}

}
func min(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}

}

func maxProfit(prices []int) int {
	if len(prices) == 0 || len(prices) == 1 {
		return 0
	}
	if len(prices) == 2 {
		return max(0, prices[1]-prices[0])
	}

	previousValue := prices[1] - prices[0]
	result := 0
	minPrice := min(prices[0], prices[1])
	for i := 2; i < len(prices); i++ {
		current := prices[i] - minPrice
		minPrice = min(minPrice, prices[i])
		result = max(current, previousValue)
		previousValue = result
	}
	return max(0, result)
}
