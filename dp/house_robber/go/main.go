package main

import "fmt"

/*
 	1) Find the maximum amount of money you can rob in n int
    2) Determine the base cases

         2,1,1,2

         2 ==>2
         2,1 ==> max(2,1) ==>2
         2,1,1
             ==> 1+2 , 1
                 max(currentvalue + rob(n-2) ,dp(n-1))
        2,1,1,2
             max(2+ 2, 3) 4,3 =>3



     3) Find the constraints two adjacent arrays are robbed




*/
func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func RobWitMem(nums []int, memo []int) int {
	elem := len(nums) - 1
	if len(nums) == 0 {
		return 0
	}
	if len(nums) == 1 {
		return nums[0]
	}
	if memo[elem] != 0 {
		fmt.Println("cache hit")
		return memo[elem]
	}

	if len(nums) == 2 {
		res := max(nums[0], nums[1])
		memo[elem] = res
		return res
	}
	recMinus2 := RobWitMem(nums[:len(nums)-2], memo)
	recMinus1 := RobWitMem(nums[:len(nums)-1], memo)
	res := max(nums[elem]+recMinus2, recMinus1)
	memo[elem] = res
	return res
}

func RobWithDp(nums []int, dp []int) int {
	if len(nums) == 0 {
		return 0
	}
	if len(nums) == 1 {
		return nums[0]
	}
	if len(nums) == 2 {
		return max(nums[0], nums[1])

	}
	dp[0] = nums[0]
	dp[1] = max(nums[0], nums[1])
	for i := 2; i < len(nums); i++ {
		dp[i] = max(nums[i]+dp[i-2], dp[i-1])
	}
	return dp[len(nums)-1]
}
func rob(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	mem := make([]int, len(nums))
	return RobWithDp(nums, mem)
}
