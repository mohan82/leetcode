package main

func isValid(arr []int, position int) bool {
	return position >= 0 && position < len(arr)
}

func canReachRecursion(arr []int, start int, visited []bool) bool {
	if !isValid(arr, start) {
		return false
	}
	if arr[start] == 0 {
		return true
	}
	jumpIndexIncr := start + arr[start]
	jumpIndexDecr := start - arr[start]
	if isValid(arr, jumpIndexIncr) && !visited[jumpIndexIncr] {
		visited[jumpIndexIncr] = true
		res := canReachRecursion(arr, jumpIndexIncr, visited)
		if res {
			return true
		}
	}
	if isValid(arr, jumpIndexDecr) && !visited[jumpIndexDecr] {
		visited[jumpIndexDecr] = true
		res := canReachRecursion(arr, jumpIndexDecr, visited)
		if res {
			return res
		}
	}
	return false
}

func canReach(arr []int, start int) bool {
	visited := make([]bool, len(arr)+1)
	return canReachRecursion(arr, start, visited)
}
