package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_canReach(t *testing.T) {
	type TestInput struct {
		arr           []int
		startPosition int
	}
	inputs := []*TestInput{&TestInput{arr: []int{4, 0}, startPosition: 0},
		&TestInput{arr: []int{4, 1, 1, 0}, startPosition: 2},
		&TestInput{arr: []int{0, 1, 1, 4, 5, 6, 7}, startPosition: 2},
		&TestInput{arr: []int{4, 2, 3, 0, 3, 1, 2}, startPosition: 5},
		&TestInput{arr: []int{4, 2, 3, 0, 3, 1, 2}, startPosition: 0},
		&TestInput{arr: []int{3, 0, 2, 1, 2}, startPosition: 2}}
	expected := []bool{false, true, true, true, true, false}
	for i := range inputs {
		assert.Equal(t, canReach(inputs[i].arr, inputs[i].startPosition), expected[i], fmt.Sprintf("test failed at index :%d", i))
	}
}
