package main

import (
	"fmt"
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

type QueueInterface struct {
	values []interface{}
}

func NewQueueInterface() *QueueInterface {
	return &QueueInterface{
		values: []interface{}{},
	}
}

func (s *QueueInterface) Enqueue(val interface{}) {
	s.values = append(s.values, val)
}

func (s *QueueInterface) Dequeue() interface{} {
	if len(s.values) == 0 {
		return 0
	}
	val := s.values[0]
	s.values = append(s.values[:0], s.values[1:]...)
	return val
}

func (s *QueueInterface) isEmpty() bool {
	return len(s.values) == 0
}

func GetBtreeLevel(index int) int {
	return int(math.Floor(float64(index) - 1/2))
}

func FindRightNodeIndex(index int) int {
	return (2 * index) + 2
}

func FindLeftNodeIndex(index int) int {
	return (2 * index) + 1
}

func FindParentIndex(index int) int {
	return GetBtreeLevel(index)
}

func IsExist(index int, arr []int) bool {
	return index > 0 && index < len(arr)
}

func insertBTree(v []int) *TreeNode {
	if len(v) == 0 {
		return nil
	}
	root := &TreeNode{Val: v[0]}
	current := root
	for i := 1; i < len(v); i++ {

		if i%2 != 0 {

		}
		if current.Left != nil && current.Right != nil {

		}

	}
	return root
}

func (t *TreeNode) print(parent int, treeType string) {
	if t == nil {
		return
	}
	t.Left.print(t.Val, "left")
	fmt.Printf("parent:%d %s:%d\n", parent, treeType, t.Val)
	t.Right.print(t.Val, "right")
}

func NewTreeNode(val int) *TreeNode {
	return &TreeNode{
		Val: val,
	}

}
func NewBtreeWithArr(arr []int) *TreeNode {
	if len(arr) == 0 {
		return nil
	}
	root := NewTreeNode(arr[0])
	q := NewQueueInterface()
	q.Enqueue(root)
	for i := 0; !q.isEmpty(); i++ {
		current := q.Dequeue().(*TreeNode)
		if leftIndex := FindLeftNodeIndex(i); IsExist(leftIndex, arr) && arr[leftIndex] != 0 {
			current.Left = NewTreeNode(arr[leftIndex])
			q.Enqueue(current.Left)
		}
		if rightIndex := FindRightNodeIndex(i); IsExist(rightIndex, arr) && arr[rightIndex] != 0 {
			current.Right = NewTreeNode(arr[rightIndex])
			q.Enqueue(current.Right)
		}
	}
	return root
}

func Test_rangeSumBST(t *testing.T) {
	inputs := [][]int{{3, 9, 20, 0, 0, 15, 7},
		{1, 0, 2},
		{}, {0}}
	expected := []int{3, 2, 0, 1}
	for i := range inputs {
		assert.Equal(t, maxDepth(NewBtreeWithArr(inputs[i])), expected[i])
	}

}
