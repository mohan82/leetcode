package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func maxDepthWithRecursion(node *TreeNode) int {
	// a nil node
	if node == nil {
		return 0
	}
	// leaf
	if node.Left == nil && node.Right == nil {
		return 1
	}
	leftDepth := maxDepthWithRecursion(node.Left)
	rightDepth := maxDepthWithRecursion(node.Right)
	return max(leftDepth, rightDepth) + 1

}
func maxDepth(root *TreeNode) int {
	return maxDepthWithRecursion(root)
}
