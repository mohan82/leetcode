package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_tribonacci(t *testing.T) {
	assert.Equal(t, tribonacci(3), 2)
	assert.Equal(t, tribonacci(4), 4)
	assert.Equal(t, tribonacci(25), 1389537)
	assert.Equal(t, tribonacci(31), 53798080)

}

func Test_tribonacciBottomUp(t *testing.T) {
	assert.Equal(t, tribonacciBottomUp(3), 2)
	assert.Equal(t, tribonacciBottomUp(4), 4)
	assert.Equal(t, tribonacciBottomUp(25), 1389537)
	assert.Equal(t, tribonacciBottomUp(31), 53798080)
	assert.Equal(t, tribonacciBottomUp(0), 0)
	assert.Equal(t, tribonacciBottomUp(1), 1)
	assert.Equal(t, tribonacciBottomUp(2), 1)

}
