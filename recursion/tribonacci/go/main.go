package main

func tribonnaciWithMemo(n int, mem []int) int {
	if mem[n] != 0 {
		return mem[n]
	}
	if n == 0 {
		return 0
	}
	if n == 1 || n == 2 {
		return 1
	}
	out := tribonacci(n-1) + tribonacci(n-2) + tribonacci(n-3)
	mem[n] = out
	return out
}

func tribonacciBottomUp(n int) int {
	memArray := make([]int, n+3)
	memArray[0] = 0
	memArray[1] = 1
	memArray[2] = 1
	for i := 3; i <= n; i++ {
		memArray[i] = memArray[i-1] + memArray[i-2] + memArray[i-3]
	}
	return memArray[n]
}
func tribonacci(n int) int {
	memArray := make([]int, n+1)
	return tribonnaciWithMemo(n, memArray)
}
