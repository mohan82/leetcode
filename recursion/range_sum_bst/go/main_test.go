package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func insert(node *TreeNode, val int) {
	if val < node.Val {
		if node.Left == nil {
			node.Left = &TreeNode{Val: val}
			return
		} else {
			insert(node.Left, val)
		}
	} else {
		if node.Right == nil {
			node.Right = &TreeNode{Val: val}
			return
		} else {
			insert(node.Right, val)
		}
	}
}

func ArrToBST(v []int) *TreeNode {
	if len(v) == 0 {
		return nil
	}
	root := &TreeNode{Val: v[0]}
	for i := 1; i < len(v); i++ {
		if v[i] == 0 {
			continue
		}
		insert(root, v[i])
	}
	return root
}

func (t *TreeNode) print(parent int, treeType string) {
	if t == nil {
		return
	}
	t.Left.print(t.Val, "left")
	fmt.Printf("parent:%d %s:%d\n", parent, treeType, t.Val)
	t.Right.print(t.Val, "right")
}

func Test_rangeSumBST(t *testing.T) {
	type TestData struct {
		inputs []int
		high   int
		low    int
	}
	inputs := []*TestData{&TestData{inputs: []int{10, 5, 15, 3, 7, 0, 18}, low: 7, high: 15},
		&TestData{inputs: []int{10, 5, 15, 3, 7, 13, 18, 1, 0, 6}, low: 6, high: 10}}
	expected := []int{32, 23}
	for i := range inputs {
		assert.Equal(t, rangeSumBST(ArrToBST(inputs[i].inputs), inputs[i].low, inputs[i].high), expected[i])
	}

}
