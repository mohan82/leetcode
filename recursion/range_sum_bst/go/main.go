package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func rangeBSTRecursion(node *TreeNode, low int, high int) int {
	if node == nil {
		return 0
	}
	v := node.Val
	sum := 0
	if v >= low && v <= high {
		sum = sum + v
	}
	sum = sum + rangeBSTRecursion(node.Left, low, high)
	sum = sum + rangeBSTRecursion(node.Right, low, high)
	return sum
}

func rangeSumBST(root *TreeNode, low int, high int) int {
	return rangeBSTRecursion(root, low, high)
}
