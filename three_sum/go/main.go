package main

import (
	"fmt"
	"sort"
)

func makeKey(i, j, k int) string {
	temp := []int{i, j, k}
	sort.Ints(temp)
	return fmt.Sprintf("%d_%d_%d", temp[0], temp[1], temp[2])
}

func threeSumWithTarget(nums []int, target int) [][]int {
	resMap := make(map[string]bool)
	var res [][]int
	for i := 0; i < len(nums); i++ {
		for j := 1; j < len(nums); j++ {
			for k := 2; k < len(nums); k++ {
				if i == j || i == k || j == k {
					continue
				}
				sum := nums[i] + nums[j] + nums[k]
				if sum == target {
					key := makeKey(nums[i], nums[j], nums[k])
					if !resMap[key] {
						resMap[key] = true
						res = append(res, []int{nums[i], nums[j], nums[k]})
					}
				}
			}
		}
	}
	return res
}

func threeSumWithTwoptr(nums []int, target int) [][]int {
	sort.Ints(nums)
	if len(nums) <= 2 {
		return [][]int{}
	}
	var res [][]int
	for i := 0; i < len(nums)-2; i++ {
		left := i + 1
		right := len(nums) - 1
		if i > 0 && nums[i] == nums[i-1] {
			continue
		}
		for left < right {
			sum := nums[left] + nums[right] + nums[i]
			if sum < target {
				left++
			} else if sum > target {
				right--
			} else {
				res = append(res, []int{nums[i], nums[left], nums[right]})
				for left < right && nums[left] == nums[left+1] {
					left++
				}
				for left < right && nums[right] == nums[right-1] {
					right--
				}
				left++
				right--
			}
		}
	}
	return res
}
