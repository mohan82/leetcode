package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ThreeSum(t *testing.T) {

	var res [][]int
	nums := [][]int{{-1, 0, 1, 2, -1, -4},
		{3, -2, 1, 0},
		{-2, 0, 1, 1, 2}}
	expected := [][][]int{{{-1, -1, 2}, {-1, 0, 1}},
		res,
		{{-2, 0, 2}, {-2, 1, 1}},
	}
	for i := range nums {
		assert.Equal(t, expected[i], threeSumWithTwoptr(nums[i], 0), fmt.Sprintf("test case  number :%d fails", i+1))
	}
}

func Test_ThreeSumWithEmptyArr(t *testing.T) {
	res := [][]int{}
	assert.Equal(t, res, threeSumWithTwoptr([]int{0}, 0))
}

func Test_ThreeSumWithThreeArgs(t *testing.T) {
	nums := []int{-1, 0, 1}
	expected := [][]int{{-1, 0, 1}}
	assert.Equal(t, threeSumWithTwoptr(nums, 0), expected)

}

func Test_ThreeSumWithTwoPtrThreeArgs(t *testing.T) {
	nums := []int{-1, 0, 1}
	expected := [][]int{{-1, 0, 1}}
	assert.Equal(t, threeSumWithTwoptr(nums, 0), expected)

}
