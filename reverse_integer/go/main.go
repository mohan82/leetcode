package main

import (
	"fmt"
	"math"
	"strconv"
)

func reverseStringToInt(s string, isPositive bool) int {
	var reversedString []byte
	for i := len(s) - 1; i >= 0; i-- {
		reversedString = append(reversedString, s[i])
	}
	if isPositive {
		result, err := strconv.Atoi(fmt.Sprintf(string(reversedString)))
		if err != nil {
			return 0
		}
		return result
	} else {
		result, err := strconv.Atoi(fmt.Sprintf("-%s", string(reversedString)))
		if err != nil {
			return 0
		}
		return result
	}
}

func powInt(x, y int) int {
	return int(math.Pow(float64(x), float64(y)))
}

func reverse(x int) int {
	res := 0
	for x != 0 {
		rem := x % 10
		x = x / 10
		res = res*10 + rem
	}
	if res > powInt(2, 31) || res < powInt(-2, 31) {
		return 0
	}
	return res
}

func reverseUsingString(x int) int {
	temp := x
	if temp < 0 {
		temp = -temp
	}
	s := fmt.Sprintf("%d", temp)
	res := reverseStringToInt(s, x > 0)
	if res > powInt(2, 31) || res < powInt(-2, 31) {
		return 0
	}
	return res
}
