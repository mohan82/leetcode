/**
 *  * @param {number} x
 *   * @return {number}
 *    */


const queue = function () {
    let arr = [];
    const enqueue = function (x) {
        arr.push(x)
    }
    const dequeue = function () {
        const val = arr[0];
        arr = arr.slice(1)
        return val;

    }
    const isEmpty = function () {
        return arr.length == 0;
    }
    const length = function () {
        return arr.length;
    }
    return {
        enqueue,
        dequeue,
        isEmpty,
        length
    };
}


// 5 --> 10 ^3  = > 5 ->10 * 10 * 10 == 5000
// 2 --> 10 ^2 ---200
// 4 -> 10 ^1  --40
// 5  =>10  ^0 - 5

var reverse = function (x) {
    let isPositive = x > 0;
    if (!isPositive) {
        x = Math.abs(x)
    }
    let current = x
    let reverseQueue = queue()

    while (current > 0) {
        let res = current / 10
        let number = (res - Math.floor(res)).toFixed(1) * 10;
        reverseQueue.enqueue(number)
        current = Math.floor(current / 10);
    }
    let result = 0
    while (!reverseQueue.isEmpty()) {
        let currentLength = reverseQueue.length();
        let number = reverseQueue.dequeue()
        result = result + number * Math.pow(10, currentLength - 1)
    }
    if (result > Math.pow(2, 31) || result < Math.pow(-2, 31)) {
        return 0
    }
    if (isPositive) {
        return result;
    } else {
        return -result;
    }

};

var reverseString = function(x) {
    let xStr = ""+x
    let reversed = xStr.split("").reverse().join("");
    if (x<0){
        reversed = "-"+reversed
    }
    let reversedNumber = parseInt(reversed);
    if ( reversedNumber > Math.pow(2,31) -1||reversedNumber < Math.pow(-2,31)) {
        return  0;
    }
    return reversedNumber;
};
console.log(reverseString(542));
console.log(reverseString(-2147483412))
console.log(reverseString(2147483648))
console.log(reverseString(-542));


