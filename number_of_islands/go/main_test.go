package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// assumes the given array is of 0's and 1's
func toByte2dArr(stringArr [][]string) [][]byte {
	var byteArr [][]byte
	for _, arr1 := range stringArr {
		var elems []byte
		for _, elem := range arr1 {
			elems = append(elems, []byte(elem)[0])
		}
		byteArr = append(byteArr, elems)
	}
	return byteArr
}

func Test_numIslands(t *testing.T) {

	grid1 := [][]string{{"1", "1", "1", "1", "0"},
		{"1", "1", "0", "1", "0"},
		{"1", "1", "0", "0", "0"},
		{"0", "0", "0", "0", "0"},
	}
	assert.Equal(t, numIslands(toByte2dArr(grid1)), 1)
	grid2 := [][]string{
		{"1", "1", "0", "0", "0"},
		{"1", "1", "0", "0", "0"},
		{"0", "0", "1", "0", "0"},
		{"0", "0", "0", "1", "1"},
	}
	assert.Equal(t, numIslands(toByte2dArr(grid2)), 3)
}
