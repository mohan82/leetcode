package main

func isRowWin(board []string, col int, c string) bool {
	isWin := true
	for row := range board {
		if string(board[row][col]) != c {
			return false
		}
	}
	return isWin
}

func isColWin(currentRow string, c string) bool {
	isWin := true
	for _, col := range currentRow {
		if string(col) != c {
			return false
		}
	}
	return isWin
}

func isDiagonalWin(board []string, c string) bool {
	diagonalXCount := 0
	diagonalYCount := 0

	for row := range board {
		for col := range board[row] {
			if row == col {
				if string(board[row][col]) == c {
					diagonalXCount++
				}
				if string(board[row][len(board[row])-(col+1)]) == c {
					diagonalYCount++
				}
			}
		}
	}
	return diagonalXCount == len(board) || diagonalYCount == len(board)
}

func isWin(board []string, c string) bool {
	for row := range board {
		if isColWin(board[row], c) {
			return true
		}
		for col := range board[row] {
			if isRowWin(board, col, c) {
				return true
			}

		}
	}
	return isDiagonalWin(board, c)
}

func validTicTacToe(board []string) bool {
	countX := 0
	countO := 0

	for _, row := range board {
		for _, col := range row {
			if string(col) == " " {
				continue
			} else if string(col) == "X" {
				countX++
			} else {
				countO++
			}
		}

	}
	if countX != countO && countO+1 != countX {
		return false
	}
	isXWin := isWin(board, "X")
	isOWin := isWin(board, "O")
	if isXWin && isOWin {
		return false
	}
	if isXWin {
		return countX == countO+1
	}
	if isOWin {
		return countX == countO
	}
	return true
}
