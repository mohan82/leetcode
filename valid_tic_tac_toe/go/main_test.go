package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_validTicTacToe(t *testing.T) {
	inputs := [][]string{[]string{"XOX", "O O", "XOX"},
		[]string{"O  ", "   ", "   "},
		[]string{"XOX", " X ", "   "},
		[]string{"XXX", "   ", "OOO"},
		[]string{"XOX", "O O", "XOX"},
		[]string{"XXX", "XOO", "OO "},
		[]string{"XXX", "OOX", "OOX"},
		[]string{"OXX", "XOX", "OXO"},
		[]string{"XXO", "XOX", "OXO"},
		[]string{"XOX", "OXO", "XXO"}}
	expected := []bool{true, false, false, false, true, false, true, false, false, true}
	for i := range inputs {
		assert.Equal(t, validTicTacToe(inputs[i]), expected[i], "Failed at index :%d", i)
	}
}
